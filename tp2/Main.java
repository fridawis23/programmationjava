import java.io.*;
import java.util.Scanner;

/*** Methode Main : Lire le fichier donné et créer un tableau 2D qui contint le characters du fichier. 
 *** Demander un mot d'utilisateur et le chercher dans le tableau 
 *** Visualiser the tableau avec le mot trouver ou informer l'utilisatuer si le mot n'est pas present */ 

public class Main {

    public static void main(String args[]) throws IOException {

	String filename;


	// Demander le nom de Fichier à l'utisateur
		filename = "//home//soultane//IdeaProjects//revision//tp2//WORDS.txt";
	// Constriure une instance de la classe "Crossword"
		Crossword crossword = new Crossword(new File(filename));


	// Demander un mot à chercher ...
		Scanner s=new Scanner(System.in);
		System.out.println("Entre un mot");
		String mot=s.nextLine();
	// Utiliser la méthode Crossword.search() pour chercher le mot
	// Ecrire un message sur le terminal pour informer l'utilisateur du resultat.
		if(crossword.search(mot)){
			System.out.println("Le mot existe dans le tableau");
			crossword.displayWord();
		}
		else{
			System.out.println("Le mot n'existe pas dans le tableau");
		}


		System.out.println("Le nombre d'occurence est de " + crossword.countWord(mot));
    }

}
