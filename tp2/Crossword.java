import javax.sound.sampled.Line;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.*;

/**
 * Class Crossword : Creates a 2D array of characters read from the input file
 */

public class Crossword
{
   private char array[][]; 	// Tableaux 2D contenant les caractères
   private int rows;      	// Nombres de lignes de array 
   private int columns;		// Nombres de colonnes de array
   
   private int WordCount;   // Nombre d'occurences d'un mot
   private int PositionX;	// Colonne de début du mot
   private int PositionY;	// Ligne de début du mot
   private int EndX;		// Colonne de fin du mot 
   private int EndY;		// Ligne de fin du mot 
   
      
   /*** Constructor: Reads each line from File <file> and writes to a new row in the array. 
    ** Updates <rows> and <columns> to height and width of the array. */
   public Crossword(File file) throws IOException {

       // Code pour calculer la taille du tableau et initialiser les variables d'instance "rows" et "columns"
       rows=0;
       Scanner scan = new Scanner(file);
       List<String> Line = new ArrayList<>();
       while(scan.hasNextLine()){
           Line.add(scan.nextLine());
           rows++;
       }
       columns =Line.get(0).length();

       //Code pour créer le tableau "array"
       array= new char[rows][columns];
	   
	   // Code pour remplir "array" avec les caractères du Fichier "file"
       for(int LineNumber = 0; LineNumber < rows; LineNumber++){
	       for(int NombreCo = 0; NombreCo< columns; NombreCo++){
	           array[LineNumber][NombreCo] = Line.get(LineNumber).charAt(NombreCo);

           }
       }
	   }

   
   /*** Methode Search(String) : Trouver le premier occurence du mot <word> dans le Tableau 
    *** Si touver, mettre à jour les valeurs (PositionY, PositionX, EndY, EndX)*/
   
   public boolean search(String word) {

       // Verifier que le taile du mot "word" est supérieure à zero. Sinon rien à faire.
       if(word.length() <= 0){
           return false;
       }

	   // Chercher le premier caractère du mot "word" dans le tableau array.
       char PremierCaractere = word.charAt(0);
       //System.out.println(PremierCaractere);


	   // Si array[i][j] contient ce caractère, alors le mot peut apparaître dans le même ligne,
        for(int i = 0; i< rows; i++){
            for(int j = 0; j<columns; j++){
                if(array[i][j] == PremierCaractere){
                    if (searchRow(i, j, word)) {
                    PositionY = i;
                    PositionX = j;
                    EndY = i;
                    EndX = j + word.length()-1;
                    return true;
                }
                    else if (searchColumn(i,j,word)){
                    PositionY = i;
                    PositionX = j;
                    EndY = i+ word.length()-1;
                    EndX = j;
                    return true;

                    }
                    else if (searchDiagonal(i,j,word)) {
                        PositionY = i;
                        PositionX = j;
                        EndY = i + word.length() - 1;
                        EndX = j+word.length()-1;
                        return true;
                    }
               }
            }
        }
	   // ou dans le même colonne. Utiliser les methodes SearchRow()
       // ou searchColumn() selon le cas.
	   return false; // mot pas trouvé
   }
   
   /* Methode Interne SearchRow(int,int, String) : Cherche une ligne du tableaux
   pour le mot <word> à partir de array[y][x] */
   
   private boolean searchRow(int y, int x, String word) {

	   // Ecrire code ici  ...
       if(word.length()   <= columns - x ){
           for(int i=1; i< word.length(); i++){
               if(array[y][i+x] != word.charAt(i)){
                   return false;
               }
           }
           return true;
       } else
           return false;
   }

   public int countWord(String word) {
       int wordCount = 0;
       if (word.length() <= 0) {
           System.out.println(false);
       }
       char Premier = word.charAt(0);
       for (int i = 0; i < rows; i++) {
           for (int j = 0; j < columns; j++) {
               if (array[i][j] == Premier) {
                   if (searchRow(i, j, word)) {
                       wordCount++;
                   } else if (searchColumn(i, j, word)) {
                       wordCount++;
                   }
                   else if (searchDiagonal(i,j,word)){
                       wordCount++;
                   }
               }
           }
       }
       return wordCount;
   }


   /* Methode Interne SearchRow(int,int, String) : Cherche une colonne du tableaux pour le mot <word> à partir de array[y][x] */
   
   private boolean searchColumn(int y, int x, String word) {

       // Ecrire code ici ...
       if (word.length() <= rows - y) {
           for (int j = 1; j < word.length(); j++) {
               if (array[y+j][x] != word.charAt(j)) {
               return false;
                }
           }
           return  true;

       }
        return false;
   }
   private boolean searchDiagonal(int y,int x,String word){
       if (word.length() <= rows - y && word.length()<= columns-x) {
           for (int j = 1; j < word.length(); j++) {
               if (array[y+j][x+j] != word.charAt(j)) {
                   return false;
               }
           }
           return  true;

       }
       return false;
   }


   /*** Methode pour visualiser le tableau. (Déjà fourni) */
   public void display() {
	   if (rows>0 && columns>0)
		   CrosswordGUI.display(array);  
	   else 
		   System.out.println("Error: Array is Empty.");
   }

   /*** Methode pour visualiser le tableau avec le mot en surbrillance. (Déjà fourni) */
   public void displayWord() {
		if ((PositionX<0) || (PositionX>EndX) || (EndX>=columns)) {
			System.out.println("Error: Incorrect x-coordinates for Word");
			return;
		}
		if ((PositionY<0) || (PositionY>EndY) || (EndY>=rows)) {
			System.out.println("Error: Incorrect y-coordinates for Word");
			return;		
		}
		CrosswordGUI.display(array, PositionY, PositionX, EndY, EndX);  
   }
   
}

